<?php namespace Fryiee\SnippetsModule;

use Anomaly\Streams\Platform\Addon\Module\Module;

/**
 * Class SnippetsModule
 * @package Fryiee\SnippetsModule
 */
class SnippetsModule extends Module
{

    /**
     * The navigation display flag.
     *
     * @var bool
     */
    protected $navigation = true;

    /**
     * The addon icon.
     *
     * @var string
     */
    protected $icon = 'fa fa-file-code-o';

    /**
     * The module sections.
     *
     * @var array
     */
    protected $sections = [
        'snippets' => [
            'buttons' => [
                'new_snippet',
            ],
        ],
    ];

}
