<?php namespace Fryiee\SnippetsModule;

use Anomaly\Streams\Platform\Addon\AddonServiceProvider;
use Fryiee\SnippetsModule\Snippet\Contract\SnippetInterface;
use Fryiee\SnippetsModule\Snippet\Contract\SnippetRepositoryInterface;
use Fryiee\SnippetsModule\Snippet\SnippetRepository;
use Anomaly\Streams\Platform\Model\Snippets\SnippetsSnippetsEntryModel;
use Fryiee\SnippetsModule\Snippet\SnippetModel;
use Illuminate\Routing\Router;

/**
 * Class SnippetsModuleServiceProvider
 * @package Fryiee\SnippetsModule
 */
class SnippetsModuleServiceProvider extends AddonServiceProvider
{

    /**
     * Additional addon plugins.
     *
     * @type array|null
     */
    protected $plugins = [
        SnippetsModulePlugin::class
    ];

    /**
     * The addon routes.
     *
     * @type array|null
     */
    protected $routes = [
        'admin/snippets'           => 'Fryiee\SnippetsModule\Http\Controller\Admin\SnippetsController@index',
        'admin/snippets/create'    => 'Fryiee\SnippetsModule\Http\Controller\Admin\SnippetsController@create',
        'admin/snippets/edit/{id}' => 'Fryiee\SnippetsModule\Http\Controller\Admin\SnippetsController@edit',
    ];

    /**
     * The addon class bindings.
     *
     * @type array|null
     */
    protected $bindings = [
        SnippetsSnippetsEntryModel::class => SnippetModel::class,
    ];

    /**
     * The addon singleton bindings.
     *
     * @type array|null
     */
    protected $singletons = [
        SnippetRepositoryInterface::class => SnippetRepository::class,
        SnippetInterface::class => SnippetModel::class,
    ];
}
