<?php namespace Fryiee\SnippetsModule;

use Anomaly\Streams\Platform\Addon\Plugin\Plugin;
use Anomaly\Streams\Platform\Support\Template;
use Fryiee\SnippetsModule\Snippet\Contract\SnippetRepositoryInterface;
use Illuminate\View\Factory;

/**
 * Class SnippetsModulePlugin
 * @package Fryiee\SnippetsModule
 */
class SnippetsModulePlugin extends Plugin
{
    /**
     * Get the plugin functions.
     *
     * @return array
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction(
                'snippet',
                function ($slug) {
                    $snippet  = (app(SnippetRepositoryInterface::class))->findBy('slug', $slug);
                    return $snippet ? (app(Factory::class))->make((app(Template::class))->make($snippet->snippet))->render() : '';
                }
            ),
        ];
    }
}