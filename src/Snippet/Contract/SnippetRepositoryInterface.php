<?php namespace Fryiee\SnippetsModule\Snippet\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface SnippetRepositoryInterface extends EntryRepositoryInterface
{

}
