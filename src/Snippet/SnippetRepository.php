<?php namespace Fryiee\SnippetsModule\Snippet;

use Fryiee\SnippetsModule\Snippet\Contract\SnippetRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class SnippetRepository extends EntryRepository implements SnippetRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var SnippetModel
     */
    protected $model;

    /**
     * Create a new SnippetRepository instance.
     *
     * @param SnippetModel $model
     */
    public function __construct(SnippetModel $model)
    {
        $this->model = $model;
    }
}
