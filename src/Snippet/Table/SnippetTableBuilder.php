<?php namespace Fryiee\SnippetsModule\Snippet\Table;

use Anomaly\Streams\Platform\Ui\Table\TableBuilder;

class SnippetTableBuilder extends TableBuilder
{

    /**
     * The table views.
     *
     * @var array|string
     */
    protected $views = [];

    /**
     * The table filters.
     *
     * @var array|string
     */
    protected $filters = [
        'name',
        'slug'
    ];

    /**
     * The table columns.
     *
     * @var array|string
     */
    protected $columns = [
        'name',
        'slug',
        'created_at' => [
            'value' => 'entry.created_at',
            'sort_column' => 'entry.created_at'
        ],
        'updated_at' => [
            'value' => 'entry.updated_at',
            'sort_column' => 'entry.updated_at'
        ],
    ];

    /**
     * The table buttons.
     *
     * @var array|string
     */
    protected $buttons = [
        'edit'
    ];

    /**
     * The table actions.
     *
     * @var array|string
     */
    protected $actions = [
        'delete'
    ];

    /**
     * The table options.
     *
     * @var array
     */
    protected $options = [];

    /**
     * The table assets.
     *
     * @var array
     */
    protected $assets = [];

}
