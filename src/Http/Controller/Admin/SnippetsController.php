<?php namespace Fryiee\SnippetsModule\Http\Controller\Admin;

use Fryiee\SnippetsModule\Snippet\Form\SnippetFormBuilder;
use Fryiee\SnippetsModule\Snippet\Table\SnippetTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class SnippetsController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param SnippetTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(SnippetTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param SnippetFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(SnippetFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param SnippetFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(SnippetFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
