<?php

return [
    'title'       => 'Snippets',
    'name'        => 'Snippets Module',
    'description' => 'Create snippets for use within themes.'
];
