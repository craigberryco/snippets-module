<?php

return [
    'snippets' => [
        'name'   => 'Snippets',
        'option' => [
            'read'   => 'Can read snippets?',
            'write'  => 'Can create/edit snippets?',
            'delete' => 'Can delete snippets?',
        ],
    ],
];
