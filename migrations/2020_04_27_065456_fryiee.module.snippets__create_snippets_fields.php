<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class FryieeModuleSnippetsCreateSnippetsFields extends Migration
{

    /**
     * The addon fields.
     *
     * @var array
     */
    protected $fields = [
        'name' => 'anomaly.field_type.text',
        'slug' => [
            'type' => 'anomaly.field_type.slug',
            'config' => [
                'slugify' => 'name',
                'type' => '_'
            ],
        ],
        'snippet'           => [
            'type'   => 'anomaly.field_type.editor',
            'config' => [
                'default_value' => '',
                'mode'          => 'twig',
            ],
        ],
    ];

}
